# Project 3: JSA

Nick Henderson

nhenders@uoregon.edu / nick@nihenderson.com

Simple anagram solving game with a word bank. Valid words are
automatically added to a list of matches, and when the total match count
is 3 (or any number specified in credentials.ini) the user is redirected
to a success page. 
